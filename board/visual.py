import collections
import dataclasses
import typing

import pygame

from .base import Board, Field, User


Color = collections.namedtuple('Color', ['r', 'g', 'b'])
Color.BLACK = Color(0, 0, 0)
Color.WHITE = Color(255, 255, 255)


@dataclasses.dataclass
class VisualUserColor:
    occupied: Color
    path: Color

    def __hash__(self):
        return hash((self.occupied, self.path))


VISUAL_USER_COLORS = {
    VisualUserColor(Color(255, 0, 0), Color(255, 153, 153)),
    VisualUserColor(Color(255, 128, 0), Color(255, 204, 153)),
    VisualUserColor(Color(255, 255, 0), Color(255, 255, 153)),
    VisualUserColor(Color(128, 255, 0), Color(204, 255, 153)),
    VisualUserColor(Color(0, 255, 255), Color(153, 255, 255)),
    VisualUserColor(Color(0, 0, 255), Color(153, 153, 255)),
    VisualUserColor(Color(128, 0, 255), Color(204, 153, 255)),
    VisualUserColor(Color(255, 0, 255), Color(255, 153, 255)),
}


class VisualUser(User):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color = self.get_color()

    @staticmethod
    def get_color() -> VisualUserColor:
        colors = next(iter(VISUAL_USER_COLORS))
        VISUAL_USER_COLORS.remove(colors)
        return colors


@dataclasses.dataclass
class VisualBoardEvent:
    field: Field
    color: Color


class VisualBoard(Board):
    TILE_SIZE = 10

    def __init__(self, height: int, width: int):
        super().__init__(height, width)

        pygame.init()
        self.surface = pygame.display.set_mode((self.width * self.TILE_SIZE, self.height * self.TILE_SIZE))
        self.changes = []
        self.drawn_changes = []

    def show_board(self) -> None:
        drawn_changes_len = len(self.drawn_changes)
        for event in self.changes[drawn_changes_len:]:
            self._show_event(event)
            self.drawn_changes.append(event)
        pygame.display.update()

    def _show_event(self, event: VisualBoardEvent) -> None:
        localization = (
            event.field.x * self.TILE_SIZE,
            event.field.y * self.TILE_SIZE,
            self.TILE_SIZE,
            self.TILE_SIZE
        )
        pygame.draw.rect(self.surface, event.color, localization)

    def _place_user(self, user: VisualUser, field: Field) -> None:
        if user.field in self.occupied[user.pid]:
            self.changes.append(VisualBoardEvent(field=user.field, color=user.color.occupied))
        if user.field in self.paths[user.pid]:
            self.changes.append(VisualBoardEvent(field=user.field, color=user.color.path))
        self.changes.append(VisualBoardEvent(field=field, color=Color.WHITE))
        super()._place_user(user, field)

    def _add_occupation(self, user: VisualUser, occ: typing.List[Field]) -> None:
        for field in occ:
            self.changes.append(VisualBoardEvent(field=field, color=user.color.occupied))
        super()._add_occupation(user, occ)

    def kill_user(self, pid: int) -> None:
        _user = self.users[pid]
        super().kill_user(pid)
        self._update_field(_user.field)

    def _remove_occupation(self, user: User) -> None:
        _occupation = self.occupied[user.pid]
        super()._remove_occupation(user)
        for field in _occupation:
            self._update_field(field)

    def _add_path(self, user: VisualUser, path: typing.List[Field]) -> None:
        for field in path:
            self.changes.append(VisualBoardEvent(field=field, color=user.color.path))
        super()._add_path(user, path)

    def _remove_path(self, user: User) -> None:
        _path = self.paths[user.pid]
        super()._remove_path(user)
        for field in _path:
            self._update_field(field)

    def _update_field(self, field: Field) -> None:
        self.changes.append(VisualBoardEvent(field=field, color=Color.BLACK))
        for pid, fields in self.occupied.items():
            if field in fields:
                self.changes.append(VisualBoardEvent(field=field, color=self.users[pid].color.occupied))
        for pid, fields in self.paths.items():
            if field in fields:
                self.changes.append(VisualBoardEvent(field=field, color=self.users[pid].color.path))
        for _, user in self.users.items():
            if field == user.field:
                self.changes.append(VisualBoardEvent(field=field, color=Color.WHITE))
