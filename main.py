import asyncio

from client.base import GameClient


SERVER = 'ws://136.243.95.36:3876'


class Run:
    def __init__(self):
        self.client = GameClient()

    async def initialize(self):
        await self.client.initialize(SERVER)

    async def visualizer(self):
        while self.client.on:
            try:
                await self.client.visualize()
            except:
                pass
            await asyncio.sleep(0.1)

    async def receiver(self):
        while self.client.on:
            try:
                messages = await self.client.recv_messages()
                await self.client.process_messages(messages)
            except Exception as err:
                print(err)
                await self.client.close()

    async def sender(self):
        while self.client.on:
            try:
                await self.client.handle_input()
            except:
                pass
            await asyncio.sleep(0.1)


async def run_game():
    run = Run()
    await run.initialize()

    tasks = [asyncio.create_task(cor) for cor in [run.visualizer(), run.receiver(), run.sender()]]
    for task in tasks:
        await task


if __name__ == '__main__':
    asyncio.run(run_game())
